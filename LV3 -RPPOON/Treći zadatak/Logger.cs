﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treći_zadatak
{
    class Logger
    {
        private static Logger instance;
        string filePath;

        private Logger()
        {
            this.filePath = @"E:\2. godina ferit\4. semestar\RPOON\lv3\prvizadatak.txt";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void Log(string message)
        {
            using (System.IO.StreamWriter writer =
                new System.IO.StreamWriter(this.filePath))
            {
                writer.WriteLine(message);
            }
        }
    }
}
