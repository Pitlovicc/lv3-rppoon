﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.zadatak
{
    class Director
    {

        public NotificationBuilder createINFO(string author)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetLevel(Category.INFO).SetAuthor(author);

            return notificationBuilder;
        }


        public NotificationBuilder createALERT(string author)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetLevel(Category.ALERT).SetAuthor(author);

            return notificationBuilder;
        }



        public NotificationBuilder createERROR(string author)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetLevel(Category.ERROR).SetAuthor(author);

            return notificationBuilder;
        }
    }
}
