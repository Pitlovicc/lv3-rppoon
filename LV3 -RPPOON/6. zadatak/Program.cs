﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Director director = new Director();
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            NotificationManager notificationManager = new NotificationManager();

            notificationBuilder = director.createINFO("Ivan");
            notificationManager.Display(notificationBuilder.Build());

        }
    }
}
