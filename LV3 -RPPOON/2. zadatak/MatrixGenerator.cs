﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.zadatak
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private RandomGenerator randomGenerator;

        private MatrixGenerator()
        {
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }

        //stvaranje matrice
        public double[][] CreateMatrix(int m, int n)
        {
            double[][] matrix = new double[m][];
            for (int i = 0; i < m; i++)
            {
                matrix[i] = new double[n];
                for (int j = 0; j < n; j++)
                {
                    matrix[i][j] = randomGenerator.NextDouble();
                    Console.Write(" ");
                    Console.Write(matrix[i][j]);
                }
                Console.WriteLine();
            }
            return matrix;
        }

    }
}
