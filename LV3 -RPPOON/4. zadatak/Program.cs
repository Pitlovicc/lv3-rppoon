﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification consoleNotification = new ConsoleNotification("Filip", "LV", "zadatak", DateTime.Now, Category.INFO, ConsoleColor.Blue);
            NotificationManager notificationManager = new NotificationManager();
            notificationManager.Display(consoleNotification);

        }
    }
}
