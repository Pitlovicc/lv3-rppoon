﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();

            ///pošto je IBuilder povratni tip, može se ovako pozvati
            notificationBuilder.SetAuthor("Filip").SetColor(ConsoleColor.Red).SetLevel(Category.INFO).SetText("text").SetTime(DateTime.Now);
            notificationBuilder.Display(notificationBuilder.Build());

        }
    }
}
